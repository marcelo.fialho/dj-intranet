DOCS = (
    ('RG', 'RG'),
    ('CPF', 'CPF'),
    ('USP', 'USP'),
    ('Passaporte', 'Passaporte'),
)

ANSWERABLE = (
    ('Pessoa1', 'Pessoa1'),
    ('Pessoa2', 'Pessoa2'),
)

STATUS = (
    ('Autorizado', 'Autorizado'),
    ('Para autorização', 'Para autorização'),
    ('Não autorizado', 'Não autorizado')
)
