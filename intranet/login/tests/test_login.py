from django.test import TestCase
from django.shortcuts import resolve_url as r
from django.contrib.auth.forms import AuthenticationForm
from django.contrib.auth.models import User


class TestLoginGet(TestCase):
    def setUp(self):
        self.resp = self.client.get(r('login'))

    def test_url(self):
        """It must return status code 200"""
        self.assertEqual(200, self.resp.status_code)

    def test_login_template(self):
        """It must render registration/login template"""
        self.assertTemplateUsed(self.resp, 'registration/login.html')

    def test_html(self):
        """It must contain login fields"""
        expected = (
            ('type="text"', 1),
            ('type="password"', 1),
            ('type="submit"', 1),
        )

        for item, count in expected:
            with self.subTest():
                self.assertContains(self.resp, item, count)

    def test_form(self):
        form = self.resp.context['form']
        self.assertIsInstance(form, AuthenticationForm)

    def test_scrf(self):
        """Html must contain csrf"""
        self.assertContains(self.resp, 'csrfmiddlewaretoken')


class TestLoginPost(TestCase):
    def setUp(self):
        User.objects.create_user(username='Marc', email='marc@email.com', password='marcpass')
        self.resp = self.client.post(r('login'), {'username': 'Marc', 'password': 'marcpass'})

    def test_post(self):
        """Status code must be 302"""
        self.assertEqual(302, self.resp.status_code)
